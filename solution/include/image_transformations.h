#pragma once
#include "../include/image.h"

enum trasformation_status {
    TRANSFORM_OK = 0,
    TRANSFORM_FAILED
};

enum trasformation_status rotate_image(struct image source, struct image* rotated_image);
