#pragma once
#include "../image.h"
#include  <stdint.h>
#include <stdio.h>

#define DEFAULT_VALUE 0
#define DEFAULT_BI_PLANES_VALUE 1
#define DEFAULT_BI_BIT_COUNT_VALUE 24
#define DEFAULT_BI_SIZE_VALUE 40
#define DEFAULT_BF_TYPE_VALUE 0x4D42 // or 424D for big-endian

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t calculate_bmp_paddings(struct image source);

struct bmp_header create_bmp_header_from_image(struct image const* source);

enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_IMAGE_DATA,
    READ_INVALID_MALLOC,
};

enum read_status from_bmp(FILE* input, struct image* img);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_IMAGE_DATA_ERROR,
};

enum write_status to_bmp( FILE* output, struct image const* img );
