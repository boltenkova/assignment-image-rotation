#pragma once
#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t height, width;
    struct pixel *data;
};

struct image struct_image_create_empty(uint32_t width, uint32_t height);

struct pixel find_pixel_address_by_coordinates(struct image const source, uint32_t i, uint32_t j);
