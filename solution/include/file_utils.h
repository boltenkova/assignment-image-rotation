#pragma once
#include "image.h"
#include <stdio.h>

/*  deserializer   */
enum open_file_status  {
    OPEN_OK = 0,
    OPEN_WRONG
};

enum file_mode {
    rb = 1,
    wb
};

enum open_file_status open_file(FILE** in, const char* current_file, int mode);

enum  close_file_status  {
    ClOSE_OK = 0,
    ClOSE_ERROR
};

enum close_file_status close_file(FILE* out);

void destructor(FILE** input, FILE** output, struct image* input_image, struct image* output_image);
