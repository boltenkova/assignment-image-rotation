#include "../include/image.h"

struct image struct_image_create_empty(uint32_t width, uint32_t height) {
    struct image new_empty_image;
    new_empty_image.height = height;
    new_empty_image.width = width;
    return new_empty_image;
}

struct pixel find_pixel_address_by_coordinates(struct image const source, uint32_t i, uint32_t j) {
    return source.data[source.width * i + j];
}
