#include "../include/image_transformations.h"
#include <stdio.h>
#include <stdlib.h>


enum trasformation_status rotate_image(struct image const source, struct image* rotated_image) {
    struct pixel* tmp_data = malloc(source.width * source.height * sizeof(struct pixel));
    for (uint32_t i = 0; i < source.height; ++i)
        for (uint32_t j = 0; j < source.width; ++j)
            tmp_data[source.height * j + (source.height - i - 1)] = find_pixel_address_by_coordinates(source, i, j);

    if (tmp_data != NULL) {
        *rotated_image = struct_image_create_empty(source.height, source.width);
        rotated_image->data = malloc(source.width * source.height * sizeof(struct pixel));
        for (uint32_t i = 0; i < source.height * source.width; ++i)
            rotated_image->data[i] = tmp_data[i];
        free(tmp_data);
        return TRANSFORM_OK;
    }
    free(tmp_data);
    return TRANSFORM_FAILED;
}

