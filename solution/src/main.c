#include "../include/bmp/bmp_format.h"
#include "../include/file_utils.h"
#include "../include/image_transformations.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

const int INVALID_NUMBER_OF_ARGUMENTS_ERROR = 1;
const int FILE_OPENING_ERROR = 2;
const int FILE_CLOSING_ERROR = 3;

int main(int argc, char** argv) {
    (void)argc; (void)argv; 
    if (argc != 3) {
        return INVALID_NUMBER_OF_ARGUMENTS_ERROR;
    }

    FILE* input = NULL;
    FILE* output = NULL;
    struct image input_image;
    struct image output_image;

    if (open_file(&input, argv[1], rb) != OPEN_OK)
        return FILE_OPENING_ERROR;

    if (open_file(&output, argv[2], wb) != OPEN_OK)
        return FILE_OPENING_ERROR;

    if (from_bmp(input, &input_image) != READ_OK)
        return FILE_CLOSING_ERROR;

    rotate_image(input_image, &output_image);

    if (to_bmp(output, &output_image) != WRITE_OK) {
        close_file(output);
        free(output_image.data);
        return FILE_CLOSING_ERROR;
    }
    
    destructor(&input, &output, &input_image, &output_image);
    return 0;
}
