#include "../../include/bmp/bmp_format.h"
#include <stdlib.h>


uint8_t calculate_bmp_paddings(struct image const source) {
    return source.width % 4 ? 4 - ((source.width * sizeof(struct pixel)) % 4) : DEFAULT_VALUE;
}

struct bmp_header create_bmp_header_from_image(struct image const* source) {
    struct bmp_header new_header = { 0 };
    new_header.bfType = DEFAULT_BF_TYPE_VALUE;
    new_header.bfReserved = DEFAULT_VALUE;
    new_header.bOffBits = sizeof(struct bmp_header);
    new_header.biSize = DEFAULT_BI_SIZE_VALUE;
    new_header.biWidth = source->width;
    new_header.biHeight = source->height;
    new_header.biPlanes = DEFAULT_BI_PLANES_VALUE;
    new_header.biBitCount = DEFAULT_BI_BIT_COUNT_VALUE;
    new_header.biCompression = DEFAULT_VALUE;
    new_header.biSizeImage = (source->width + calculate_bmp_paddings(*source)) * source->height;
    new_header.biXPelsPerMeter = DEFAULT_VALUE;
    new_header.biYPelsPerMeter = DEFAULT_VALUE;
    new_header.biClrUsed = DEFAULT_VALUE;
    new_header.biClrImportant = DEFAULT_VALUE;
    new_header.bfileSize = sizeof(struct bmp_header) + new_header.biSizeImage;
    return new_header;
}

enum read_status from_bmp(FILE* input, struct image* img) {
    // read bmp header
    struct bmp_header current_bmp_header = { 0 };
    if (!fread(&current_bmp_header, sizeof(struct bmp_header), 1, input)) {
        return READ_INVALID_HEADER;
    }

    // read data from bmp to struct image
    *img = struct_image_create_empty(current_bmp_header.biWidth, current_bmp_header.biHeight);
    img->data = malloc(current_bmp_header.biWidth * current_bmp_header.biHeight * sizeof(struct pixel));
    fseek(input, (long)current_bmp_header.bOffBits, SEEK_SET);

    const uint8_t image_padding = calculate_bmp_paddings(*img);
    for (size_t i = 0; i < img->height; ++i) {
        if (!fread(&(img->data[img->width * i]), img->width * sizeof(struct pixel), 1, input)) {
            return READ_INVALID_IMAGE_DATA;
        }
        fseek(input, image_padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* output, struct image const* img) {
    struct bmp_header out_bmp_header = create_bmp_header_from_image(img);
    if (!fwrite(&out_bmp_header, sizeof(struct bmp_header), 1, output)) {
        return WRITE_HEADER_ERROR;
    }

    const char garbage[4] = { 0 };
    uint8_t image_padding = calculate_bmp_paddings(*img);
    for (size_t i = 0; i < img->height; ++i) {
        if (!fwrite(&(img->data[img->width * i]), img->width * sizeof(struct pixel), 1, output)) {
            return WRITE_IMAGE_DATA_ERROR;
        }
        fwrite(&garbage, image_padding, 1, output);
    }

    return WRITE_OK;
}
