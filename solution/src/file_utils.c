#include "../include/file_utils.h"
#include <stdio.h>


enum open_file_status open_file(FILE** input, const char* current_file, int mode) {
    if (mode == 1)
        *input = fopen(current_file, "rb");
    else if (mode == 2)
        *input = fopen(current_file, "wb");
    else
        return OPEN_WRONG;
    if (*input == NULL) {
        return OPEN_WRONG;
    }
    return OPEN_OK;
}

enum close_file_status close_file(FILE* output) {
    if (!fclose(output)) {
        return ClOSE_ERROR;
    }
    return ClOSE_OK;
}


void destructor(FILE** input, FILE** output, struct image* input_image, struct image* output_image) {
    close_file(*input);
    free(input_image->data);
    close_file(*output);
    free(output_image->data);
}
